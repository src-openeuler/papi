Name:            papi
Version:         7.1.0
Release:         3
Summary:         Performance Application Programming Interface
License:         BSD-3-clause
URL:             http://icl.cs.utk.edu/papi/
Source0:         http://icl.cs.utk.edu/projects/papi/downloads/%{name}-%{version}.tar.gz
BuildRequires:   autoconf doxygen ncurses-devel gcc-gfortran kernel-headers >= 2.6.32
BuildRequires:   chrpath lm_sensors-devel libpfm-devel >= 4.6.0-1 libpfm-static >= 4.6.0-1
BuildRequires:   net-tools rdma-core-devel perl-generators
Requires:        python3-unversioned-command
Provides:        papi-libs = %{version}-%{release}
Obsoletes:       papi-libs < %{version}-%{release}

Patch0001:       papi-add-support-riscv64.patch
Patch0002:	 add-loongarch64-support-for-papi.patch

%description
PAPI provides a programmer interface to monitor the performance of
running programs,and contains the run-time libraries for any application that wishes
to use PAPI.

%package         devel
Summary:         Header files for the compiling programs with PAPI
Requires:        papi = %{version}-%{release} pkgconfig
Provides:        papi-testsuite = %{version}-%{release} papi-static = %{version}-%{release}
Obsoletes:       papi-testsuite < %{version}-%{release} papi-static < %{version}-%{release}

%description     devel
PAPI-devel provides C header files for specifying PAPI user-space libraries and interfaces,
a test testuiste for checking PAPI functionality, and static libraries for compiling programs with PAPI.

%package        help
Summary:        Help documents for papi

%description    help
The papi-help package conatins manual pages and documents for papi.

%prep
%autosetup -p1

%build
cd src
autoconf
%configure --with-perf-events --with-pfm-incdir=%{_includedir} --with-pfm-libdir=%{_libdir} \
--with-static-lib=yes --with-shared-lib=yes --with-shlib --with-shlib-tools \
--with-components="appio coretemp example infiniband lmsensors lustre micpower mx net rapl stealtime"


DBG="" make %{?_smp_mflags}
cd ../doc
DBG="" make
DBG="" make install

%install
cd src
make DESTDIR=$RPM_BUILD_ROOT LDCONFIG=/bin/true install-all

chrpath --delete $RPM_BUILD_ROOT%{_libdir}/*.so*
file `find $RPM_BUILD_ROOT%{_bindir} -type f` |grep -w ELF |awk -F":" '{print $1}'|for file in `xargs`
do
chrpath --delete $file
done

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc LICENSE.txt
%{_bindir}/*
%{_libdir}/*.so.*
%dir /usr/share/papi
/usr/share/papi/papi_events.csv

%files devel
%{_includedir}/*.h
%{_includedir}/*.hpp
%{_libdir}/*.so
%{_libdir}/pkgconfig/papi*.pc
%{_libdir}/*.a
/usr/share/papi/*
%exclude /usr/share/papi/papi_events.csv

%files help
%doc INSTALL.txt README.md RELEASENOTES.txt
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Fri Oct 25 2024 laokz <zhangkai@iscas.ac.cn> - 7.1.0-3
- Riscv64: fix 'rdcycle' instruction to 'rdtime'

* Tue Apr 16 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 7.1.0-2
- add loongarch64 support for papi

* Tue Jan 2 2024 liyanan <liyanan61@h-partners.com> - 7.1.0-1
- Upgrade to version 7.1.0

* Mon Aug 28 2023 chenchen <chen_aka_jan@163.com> - 7.0.1-2
- remove binaries runpath & rpath

* Tue Jun 20 2023 Ge Wang <wang__ge@126.com> - 7.0.1-1
- Upgrade to version 7.0.1

* Wed Oct 26 2022 hua <dchang@zhixundn.com> - 6.0.0-1
- upgrade version to 6.0.0

* Mon Jan 24 2022 wujie <wujie@nj.iscas.ac.cn> - 5.6.0-10
- add support for riscv64 from http://fedora.riscv.rocks/koji/buildinfo?buildID=31200
  Author is David Abdurachmanov <david.abdurachmanov@gmail.com>

* Tue Jul 28 2020 lingsheng<lingsheng@huawei.com> - 5.6.0-9
- remove buildrequire infiniband-diags-devel

* Thu Nov 28 2019 liujing<liujing144@huawei.com> - 5.6.0-8
- Package init
